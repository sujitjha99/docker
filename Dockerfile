FROM ubuntu:latest
RUN apt-get update
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get install -y nano
RUN apt-get install -y net-tools
RUN apt-get install -y apache2
Run apt-get install -y apache2-utils
Run apt-get install -y \
	libapache2-mod-php7.2 \
	php7.2 \
	php-mysql
RUN apt-get install -y \ 
	mysql-server \
	mysql-client \
	mysql-common
ENV DEBIAN_FRONTEND=teletype
EXPOSE 80
COPY noby.html file1.html pattern.css file1.php connect.php index.html  welcome.php /var/www/html/
COPY service.sh /
CMD ["sh", "-c", "./service.sh"]
COPY mysql.sh /


